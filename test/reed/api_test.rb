require File.expand_path("test/test_helper")
require File.expand_path("lib/reed/api")

module Reed
  class APITest < Minitest::Test
    def setup
      @reed = API.new
      @keywords = "web developer"
    end

    def test_getting_one_job
      jobs = @reed.job_search(@keywords, limit: 1)
      assert_equal 1, jobs.size
    end

    def test_getting_five_jobs
      jobs = @reed.job_search(@keywords, limit: 5)
      assert_equal 5, jobs.size
    end

    def test_getting_fifty_jobs
      jobs = @reed.job_search(@keywords, limit: 50)
      assert_operator jobs.size, :<, 50
    end

    def test_getting_non_temporary_job
      jobs = @reed.job_search(@keywords, limit: 1, temporary: false)
      assert_equal 1, jobs.size
    end

    def test_literal_search
      jobs = @reed.job_search(@keywords, variations: nil, limit: 1)
      assert_equal 1, jobs.size
    end

    def test_passing_in_a_callback_to_job_search
      job = nil
      @reed.job_search(@keywords, limit: 1) do |j|
        job = j
      end
      refute_nil job
    end
  end
end
