# Reed::API

reed.co.uk job search API wrapper

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'reed-api', git: 'git@bitbucket.org:cristian-rasch/reed-api.git', require: 'reed/api'
```

And then execute:

    $ bundle

## Usage

```ruby
require "reed/api"
require "pp"

reed = Reed::API.new # the API key is read from the REED_API_KEY env var
# Options include:
#   - api_key - the reed.co.uk API key to use
#   - temporary true/false - whether to look for temporary jobs (defaults to true)
#   - limit - how many jobs to retrieve (defaults to 100, max. 100)
#   - per_hour - highest possible hourly rate, e.g. 300
#   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
#                  defaults to ["remote", "telecommute", "telecommuting", "work from home", "contract", "freelance"]
#   - logger - a ::Logger instance to log error messages to
jobs = reed.job_search("web developer", limit: 5) # returns up to 5 jobs
pp jobs.first
# #<struct Reed::API::Job
#  id=26269349,
#  employer="Cantium Clothing Ltd",
#  title="IT & E-Commerce Specialist",
#  location="Manston Park",
#  minimum_salary=20000.0,
#  maximum_salary=20000.0,
#  currency="GBP",
#  expires_on=#<Date: 2015-01-02 ((2457025j,0s,0n),+0s,2299161j)>,
#  created_on=#<Date: 2015-02-14 ((2457068j,0s,0n),+0s,2299161j)>,
#  description=
#   " Location: Ramsgate, Kent. Contract: Temporary to permanent after 6 month review period. Salary: &#163;20,000 &#43; 6 monthly performance &amp; profit related bonus. The Company: Cantium Clothing Ltd. is a small family run online retailer of branded footwear, based out of a single warehouse facility near Ramsgate in Kent. The Role: We have a vacancy for a motivated professional with experience relevan... ",
#  applications_count=19,
#  url="http://www.reed.co.uk/jobs/26269349">
```
