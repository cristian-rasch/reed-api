require_relative "api/version"

require "dotenv"
Dotenv.load
require "net/http"
require "cgi"
require "json"
require "set"
require "logger"

module Reed
  class API
    class Job < Struct.new(:id, :employer, :title, :location, :minimum_salary,
                           :maximum_salary, :currency, :expires_on, :created_on,
                           :description, :applications_count, :url)
    end

    BASE_URL = "http://www.reed.co.uk/api/1.0/search?keywords=:keywords".freeze
    DATE_FORMAT = "%d/%m/%Y".freeze
    DEFAULT_TEMP = true
    DEFAULT_LIMIT = 100
    DEFAULT_VARIATIONS = ["remote", "telecommute", "telecommuting", "work from home", "contract", "freelance"].freeze

    # Options include:
    #   - api_key - the reed.co.uk API key to use
    #   - temporary true/false - whether to look for temporary jobs (defaults to true)
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - per_hour - highest possible hourly rate, e.g. 300
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - logger - a ::Logger instance to log error messages to
    def initialize(options = {})
      @options = options
    end

    # Options include:
    #   - api_key - the reed.co.uk API key to use
    #   - temporary true/false - whether to look for temporary jobs (defaults to true)
    #   - limit - how many jobs to retrieve (defaults to 100, max. 100)
    #   - per_hour - highest possible hourly rate, e.g. 300
    #   - variations - a list of suffixes to append to the searched keywords, e.g. %w(remote freelance contract)
    #   - logger - a ::Logger instance to log error messages to
    #   - callback - an optional block to be called with each job found ASAP
    #
    # Returns: a collection of up to limit Job objects on successful requests
    def job_search(keywords, options = {})
      configure_logger(options[:logger])

      url = BASE_URL.dup
      temporary = options[:temporary] || @options[:temporary] || DEFAULT_TEMP
      url << "&temp=true" if temporary

      limit = options[:limit] || @options[:limit] || DEFAULT_LIMIT
      limit = [limit, DEFAULT_LIMIT].min
      url << "&resultsToTake=#{limit}"
      offset = limit*-1

      per_hour = options[:per_hour] || @options[:per_hour]
      url << "&perHour=#{per_hour.to_i}" if per_hour

      variations = if options.has_key?(:variations)
                     options[:variations]
                   elsif @options.has_key?(:variations)
                     @options[:variations]
                   else
                     DEFAULT_VARIATIONS
                   end
      variations = Array(variations)
      variations << "" if variations.empty?
      keywords = Array(keywords)
      api_key = options[:api_key] || @options[:api_key] || ENV.fetch("REED_API_KEY")
      job_ids = Set.new
      jobs = []

      catch(:done) do
        begin
          offset += limit
          search_url = url.dup
          search_url << "&resultsToSkip=#{offset}" if offset > 0

          empty = variations.map do |variation|
            kws = keywords.dup
            kws << variation
            api_url = search_url.sub(":keywords", CGI.escape(kws.join(" ")))
            uri = URI(api_url)

            req = Net::HTTP::Get.new(uri)
            req.basic_auth(api_key, "")

            begin
              res = Net::HTTP.start(uri.hostname, uri.port) { |http|
                http.request(req)
              }
            rescue => err
              @logger.error "#{err.message} opening '#{uri}'"
              next(true)
            else
              case res
              when Net::HTTPSuccess then
                body = JSON.parse(res.body)
                results = body["results"]
                if results.empty?
                  true
                else
                  results.each do |hash|
                    created_on = parse_date(hash["date"])
                    expires_on = parse_date(hash["expirationDate"])
                    id = hash["jobId"]

                    unless job_ids.include?(id)
                      job = Job.new(id, hash["employerName"], hash["jobTitle"], hash["locationName"],
                                    hash["minimumSalary"], hash["maximumSalary"], hash["currency"],
                                    created_on, expires_on, hash["jobDescription"], hash["applications"],
                                    hash["jobUrl"])
                      yield(job) if block_given?
                      job_ids << id
                      jobs << job
                    end

                    throw(:done) if job_ids.size == limit
                  end

                  false
                end
              else
                @logger.error "#{err.body} opening '#{uri}'"
                next(true)
              end
            end
          end

          done = empty.all?
        end until done
      end

      jobs
    end

    private

    def configure_logger(logger = nil)
      @logger = logger || @options[:logger] || Logger.new(STDERR)
    end

    def parse_date(date)
      Date.strptime(date, DATE_FORMAT) if date
    end
  end
end
